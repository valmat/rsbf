/**
 *  Random shuffle big file
 *
 *  @author valmat <ufabiz@gmail.com>
 *  @repo https://bitbucket.org/valmat/rsbf/
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <unistd.h>   // getopt
#include <algorithm>  // std::random_shuffle
#include <ctime>      // std::time
#include <cstdlib>    // std::rand, std::srand



void shuffle_file(std::istream& input, size_t num_lines, size_t skip_lines)
{
    std::vector<std::string> inputs;

    // Is stream iterator valid
    bool valid = true;
    
    size_t nums = 0;
    std::string line;

    // Skip lines
    while( (nums < skip_lines) && (valid = static_cast<bool>(std::getline(input, line))) ) {
        std::cout << line << '\n';
        ++nums;
    }
    std::cout << std::flush;

    while(valid) {
        inputs.clear();
        inputs.reserve(num_lines);

        nums = 0;
        while( (nums < num_lines) && (valid = static_cast<bool>(std::getline(input, line))) ) {
            inputs.emplace_back(std::move(line));
            ++nums;
        }
        // shuffle input vector
        std::random_shuffle (inputs.begin(), inputs.end(), [](size_t i) -> size_t { return std::rand() % i;});
        for (auto &&v: inputs) {
            std::cout << v << '\n';
        }
        std::cout << std::flush;
    }
}

#define HELP_EXIT()  print_help(*argv); return 0;
void print_help(const char *script_name)
{
    std::cerr << "\033[1;33mUsege: \033[1;32m" << script_name << "\033[1;34m -n<number lines> [-s<skip lines>] [-i<input filename>]\033[0m";
    std::cerr << std::endl << "exemple: " << script_name << " -n1000000 -i-";
    std::cerr << std::endl << "Random shuffle big file";
    std::cerr << std::endl << "";
    std::cerr << std::endl << "-n \tNumber of lines (> 0) that will be shuffled at the same time.";
    std::cerr << std::endl << "-s \tHow many lines to skip (0 by default).";
    std::cerr << std::endl << "-i \tInput file name (ctdin by default). If `-` will be used stdin.";
    std::cerr << std::endl << "-h \tPrint current help and exit.";
    std::cerr << std::endl;
}

int main(int argc, char **argv)
{
    std::string in_fname;
    size_t num_lines  = 0;
    size_t skip_lines = 0;

    //
    // Get options
    //
    char copt=0;
    unsigned optcnt = 0;
    opterr=0;
    while ( (copt = getopt(argc,argv,"n:i::s::h")) != -1) {
        ++optcnt;
        switch (copt){
            case 'h':
                HELP_EXIT();
            break;
            case 'n':
                if(optarg) {
                    num_lines  = std::strtoull(optarg, nullptr, 10);
                }
            break;
            case 's':
                if(optarg) {
                    skip_lines = std::strtoull(optarg, nullptr, 10);
                }
            break;
            case 'i':
                if(optarg) {
                    in_fname   = optarg;
                }
            break;
            case '?':
                HELP_EXIT();
            break;
        };
    };
    if(optcnt < 1 || 0 == num_lines) {
        HELP_EXIT();
    }

    // random seed
    std::srand( size_t( std::time(0) ) );

    //
    // Input stream
    //
    if(in_fname.empty() || "-" == in_fname) {
        shuffle_file(std::cin, num_lines, skip_lines);
    } else {
        // Check if input data are valid
        std::ifstream source_file(in_fname, std::ios::in | std::ios::binary);
        if(!source_file) {
            std::cerr << "Can't open file \033[1;31m" << in_fname << "\033[0m" << std::endl;
            return 1;
        }
        shuffle_file(source_file, num_lines, skip_lines);
    }
    return 0;
}