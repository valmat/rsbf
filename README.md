# Random shuffle big file #


### Allows to shuffle a large file, if it does not fit in memory

### Compile
Just run `make`

## Usage

```
./rsbf -n<number lines> [-s<skip lines>] [-i<input filename>]
exemple: ./rsbf -n1000000 -i-

-n 	Number of lines (> 0) that will be shuffled at the same time.
-s 	How many lines to skip (0 by default).
-i 	Input file name (ctdin by default). If `-` will be used stdin.
-h 	Print current help and exit.
```

Example of usage:
```bash
#!/bin/sh

# limit 1024×1024×10 = 10485760
ulimit -Sv 10485760

./rsbf -n500000          -i"bigfile.txt"   > "bigfile-1.txt"
./rsbf -n500000 -s250000 -i"bigfile-1.txt" > "bigfile-2.txt"
./rsbf -n500000 -s0      -i"bigfile-2.txt" > "bigfile-1.txt"
./rsbf -n500000 -s250000 -i"bigfile-1.txt" > "bigfile-2.txt"
./rsbf -n500000 -s0      -i"bigfile-2.txt" > "bigfile-1.txt"
./rsbf -n500000 -s150000 -i"bigfile-1.txt" > "bigfile.txt"
rm "bigfile-1.txt"
rm "bigfile-2.txt"
```
#### The MIT License
