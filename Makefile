CPP   = g++
LD    = g++
#CPP   = clang++
#LD    = clang++



#compiler flags
OPT_FLAG   = -O3 -pedantic -Wall -Wextra
CPP_FLAGS  = -I. ${OPT_FLAG} -std=${CPP_STD} -fno-exceptions -c
LD_FLAGS   = -std=${CPP_STD} ${OPT_FLAG}
CPP_STD    = c++11

##############################
SOURCES    = rsbf.cpp
OBJECTS    = o/rsbf.o
##############################


all:	rsbf
	@echo
	@echo "\033[1;36mBuild complite \033[0m"

o/rsbf.o: rsbf.cpp
	${CPP} ${CPP_FLAGS} -o o/rsbf.o rsbf.cpp


.PHONY: obj

obj: $(OBJECTS)


rsbf: o/rsbf.o
		${LD} ${LD_FLAGS} -o rsbf o/rsbf.o

.PHONY: clean

clean:
		rm -f o/*.o rsbf
